import React from 'react';
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import { createStore, combineReducers } from 'redux'
import axios from 'axios'

import dataReducer from './store/reducers/data'
import Routes from './containers/Routes'

axios.defaults.baseURL = "http://www.mocky.io/v2"

const rootReducers = combineReducers({ // Puede contener muchos reducers
  data: dataReducer
})
const store = createStore(rootReducers)

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Routes />
      </BrowserRouter>
    </Provider> 
  );
}

export default App;
