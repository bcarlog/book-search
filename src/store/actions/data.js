export const CHANGE_DATA = 'CHANGE_DATA'

export const changeData = (data) => {
    return { type: CHANGE_DATA, data: data }
}