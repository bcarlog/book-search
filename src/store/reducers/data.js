import { CHANGE_DATA } from '../actions/data'

const initializeState = {
    data: localStorage.getItem('data') || null
}

const dataReducer = (state = initializeState, action) => {
    switch (action.type) {
        case CHANGE_DATA:
            const newData = action.data
            return { ...state, data: newData }
        default:
            return state
    }
}

export default dataReducer