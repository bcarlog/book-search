import React from 'react'
import { Route, Switch, } from 'react-router-dom'

import Home from './Home/Home'

const Routes = (props) => {
    return (
        <Switch>
            <Route path="/" exact component={Home} />
            <Route render={()=><h1>Not found</h1>}/>
        </Switch>
    )
}

export default Routes