import React, { useEffect, useState } from 'react'
import axios from 'axios'
import classes from './Home.module.sass'

import Header from '../../components/Header/Header'
import Book from '../../components/Book/Book'

const Home = props => {

    const [books, setBooks] = useState([])
    const [filterBooks, setFilterBooks] = useState([])
    const [authors, setAuthors] = useState([])

    useEffect(()=>{
        axios.get('5e1683a23000004d00d56089')
            .then(response => {
                setBooks(response.data)
                setFilterBooks(response.data)
            })
        axios.get('5e1684a93000002c00d5608e')
            .then(response => {
                setAuthors(response.data)
            })
    },[])

    const searchHandler = (text) => {
        const filter = books.filter(book => book.title.toUpperCase().indexOf(text.toUpperCase()) !== -1)
        setFilterBooks(filter)
    }

    if(books.length === 0 || authors.length === 0){
        return <div>Loading...</div>
    }

    return (
        <div className={classes.home}>
            <Header onSearch={searchHandler}/>
            {filterBooks.map((book,i) => (
                <Book key={`Book_${i}`} authors={authors} book={book}/>
            ))}
        </div>
    )
}

export default Home