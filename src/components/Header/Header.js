import React from 'react'
import classes from './Header.module.sass'

const Header = props => {
    return (
        <div className={classes.header}>
            <h1 className={classes.title}>Best Sellers all times</h1>
            <input  
                className={classes.search} placeholder='write something to search'
                onChange={(event)=>{props.onSearch(event.target.value)}}
            />
        </div>
    )
}

export default Header