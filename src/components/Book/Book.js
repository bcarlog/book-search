import React, { useState, useEffect } from 'react'
import classes from './Book.module.sass'

const Book = state => {
    const [book, setBook] = useState(state.book)
    const [authors, setAuthors] = useState(state.authors)

    useEffect(()=>{
        setBook(state.book)
    },[state.book])
    
    const author = authors.find(author => author.id === book.authorId)

    return (
        <div className={classes.book}>
            <div className={classes.head}>
                <div className={classes.title}>{book.title.toUpperCase()}</div>
                <div className={classes.authorid}>AUTHORID: {book.authorId}</div>
            </div>
            <div className={classes.body}>
                <div className={classes.authorName}>{author.name}</div>
                <div className={classes.description}>{book.description}</div>
            </div>
            <div className={classes.buttons}>
                <div className={classes.show}>SHOW BOOK</div>
            </div>

        </div>
    )
}

export default Book